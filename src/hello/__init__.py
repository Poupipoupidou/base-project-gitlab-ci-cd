""" This package contains modules for greeting functions. """
from .hello_funs import say_hello, say_happy_new_year, say_something_to_someone
